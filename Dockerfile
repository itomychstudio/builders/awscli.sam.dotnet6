FROM alpine:latest

RUN apk update

RUN apk add --no-cache \
            python3-dev \
            py3-pip \
            gcc \
            musl-dev \
			zip \
            bash \
            icu-libs \
            libssl1.1

RUN pip3 install awscli --no-cache-dir --upgrade
RUN pip3 install aws-sam-cli --no-cache-dir --upgrade
RUN pip3 install yamllint --no-cache-dir --upgrade

RUN mkdir -p /usr/share/dotnet \
    && ln -s /usr/share/dotnet/dotnet /usr/bin/dotnet

RUN wget https://dot.net/v1/dotnet-install.sh
RUN chmod +x dotnet-install.sh
RUN ./dotnet-install.sh -c 5.0 --install-dir /usr/share/dotnet
RUN ./dotnet-install.sh -c 6.0 --install-dir /usr/share/dotnet
